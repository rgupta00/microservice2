package com.order.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.order.dto.Coupon;
import com.order.dto.Customer;
import com.order.dto.Order;
import com.order.dto.OrderRequest;
import com.order.dto.Product;

@RestController
public class OrderController {

	@Autowired
	private RestTemplate restTemplate;
	//http://localhost:8080/order/api/order?pid=1&cid=1&couponcode=SUP10
	@PostMapping(value = "/api/order")
	public ResponseEntity<Order> submitOrder(@RequestBody OrderRequest orderRequest ){
		//first of all i wall call prdouct ms to get the product
		
		String productUrl="http://localhost:8082/product/api/product/"+ orderRequest.getProductId();
		
		String customerUrl="http://localhost:8081/customer/api/customer/"+ orderRequest.getCustomerId();
		
		String couponUrl="http://localhost:8085/coupon/api/coupon/"+ orderRequest.getCouponcode();
		
		//i will call customer ms to get the customer
		
		//i will coupon service to get the discount
		
		Product product=restTemplate.getForObject(productUrl, Product.class);
		Customer customer=restTemplate.getForObject(customerUrl, Customer.class);
		Coupon coupon=restTemplate.getForObject(couponUrl, Coupon.class);
		
		Order order=new Order();
		order.setId(545454L);
		order.setProduct(product);
		order.setCustomer(customer);
		int price= product.getPrice()-product.getPrice()*10;
		order.setAmount(price);
		order.setDateOrder(new Date());
	
		
		return new ResponseEntity<Order>(order, HttpStatus.CREATED);
	}
	
}
