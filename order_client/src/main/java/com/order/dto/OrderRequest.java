package com.order.dto;

public class OrderRequest {
	private int customerId;
	private int productId;
	private String couponcode;
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getCouponcode() {
		return couponcode;
	}
	public void setCouponcode(String couponcode) {
		this.couponcode = couponcode;
	}
	public OrderRequest(int customerId, int productId, String couponcode) {
		this.customerId = customerId;
		this.productId = productId;
		this.couponcode = couponcode;
	}
	public OrderRequest() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
