package com.customer;
import java.util.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.customer.service.Customer;

@SpringBootApplication
public class CustomerServiceApplication {

	public static void main(String[] args) {
		 
		SpringApplication.run(CustomerServiceApplication.class, args);
	}

}
