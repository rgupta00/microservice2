package com.customer.controller;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.customer.service.Customer;
import com.customer.service.CustomerService;

@RestController
public class CustomerController {

	private CustomerService customerService;

	@Autowired
	public CustomerController(CustomerService customerService) {
		this.customerService = customerService;
	}
	
	@GetMapping(path = "api/customer/{id}")
	public Customer getAnCustomer(@PathVariable(name = "id") int id){
		return customerService.getCustomerById(id);
		
	}
	

	@GetMapping(path = "api/customer")
	public List<Customer>getAll(){
		return customerService.getAllCustomers();
		
	}
	
	
	
}
