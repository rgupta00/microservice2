package com.customer.service;

import java.util.List;

public interface CustomerService {
	public List<Customer> getAllCustomers();
	public Customer getCustomerById(int id);
}