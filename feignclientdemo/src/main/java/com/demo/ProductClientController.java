package com.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ProductClientController {
	
	@Autowired
	private ProductClient productClient;
	
	
	@GetMapping("callProductService")
	public String callProductService() {
		String productInfor=productClient.getProductInformation();
		return productInfor;
	}
}
