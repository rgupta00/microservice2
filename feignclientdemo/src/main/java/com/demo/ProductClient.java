package com.demo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("PRODUCT-SERVICE")
public interface ProductClient {
	@GetMapping("/product")
	public String getProductInformation();
}
