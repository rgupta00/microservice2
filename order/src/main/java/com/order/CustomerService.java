package com.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class CustomerService {

	@Autowired
	private RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod = "defaultcustomer")
	public String customerService() {
		String customerInfor=restTemplate.getForObject("http://CUSTOMER-SERVICE/customer",String.class);
		return customerInfor;
	}
	
	
	
	public String defaultcustomer() {
		return "default customer";
	}

}
