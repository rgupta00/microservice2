package com.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class ProductService {

	@Autowired
	private RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod = "defaultproduct")
	public String productService() {
		String productInfor=restTemplate.getForObject("http://PRODUCT-SERVICE/product", String.class);
		return productInfor;
	}
	
	public String defaultproduct() {
		return "default product";
	}
}

