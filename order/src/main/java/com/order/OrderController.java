package com.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@RestController
public class OrderController {
	

	@Autowired
	private ProductService productService;
	
	@Autowired
	private CustomerService customerService;
	
	
	@RequestMapping("/order")
	public String HelloWorld() {
		String customerInfor = customerService.customerService();
		String productInfor = productService.productService();
		
		return customerInfor+ ": "+ productInfor;
	}

	
}
