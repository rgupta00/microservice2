package com.coupon.service;
public interface CouponService {
	public Coupon getCouponByCode(String couponCode);
}