package com.coupon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.coupon.service.Coupon;
import com.coupon.service.CouponService;

@RestController
public class CouponController {
	
	private CouponService couponService;

	@Autowired
	public CouponController(CouponService couponService) {
		this.couponService = couponService;
	}
	

	@GetMapping(value = "/api/coupon/{couponcode}")
	public ResponseEntity<Coupon> getAnCoupon(@PathVariable String couponcode) {
		Coupon coupon = couponService.getCouponByCode(couponcode);
		if (coupon==null) {
			return new ResponseEntity<Coupon>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Coupon>(coupon, HttpStatus.OK);
	}

}
