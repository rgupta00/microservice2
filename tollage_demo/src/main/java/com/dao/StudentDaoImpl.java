package com.dao;
import java.sql.*;

import java.util.*;

public class StudentDaoImpl implements StudentDao{

	private Connection connection;
	
	
	public StudentDaoImpl() {
		connection=ConnectionFactory.getConnection();
	}
	@Override
	public List<Student> getAllStudents() {
		List<Student> students=new ArrayList<>();
		Student student;
		
		try {
			PreparedStatement pstmt = connection.prepareStatement("select * from student");
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				student =new Student
						(rs.getInt(1), rs.getString(2), rs.getInt(3));
				
				students.add(student);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return students;
	
	}

}
