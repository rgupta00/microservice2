package com.dao;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory {
	private ConnectionFactory() {
	}

	private static Connection connection = null;

	public static Connection getConnection() {
		Properties properties = new Properties();
		String url = null, username = null, password = null, driverName = null;

		try {
			InputStream is = new FileInputStream("db.properties");
			properties.load(is);// what it does it seprate key and value part and
			/// then we can use properties and get the value corrosponding to that keys

			driverName = properties.getProperty("jdbc.driverName");
			url = properties.getProperty("jdbc.url");
			username = properties.getProperty("jdbc.username");
			password = properties.getProperty("jdbc.password");

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Class.forName(driverName);
			System.out.println("driver is loaded");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(url, username, password);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return connection;
	}
}
