package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
public class ConfigdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigdemoApplication.class, args);
	}

}
