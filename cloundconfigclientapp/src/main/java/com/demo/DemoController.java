package com.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RefreshScope
@RestController
public class DemoController {
	
	@Value("${spring.datasource.url:not found}")
	private String url;
	
	@GetMapping(path = "hello")
	public String hello() {
		return url;
	}

}
