package com.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoContoller {
	@Value("${hello.myval2:some alternative value}")
	private String myVal;
	
	@Value("${hello.message}")
	private String message;
	
	@Value("${hello.message2}")
	private String message2;
	
	@Value("${spring.datasource.url}")
	private String url;
	
	@GetMapping(path = "hello")
	public String hello() {
		System.out.println(url);
		return message+": "+message2+ ": "+ myVal;
	}
	
}
