package com.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController4 {

	@Autowired
	private DbConfig dbConfig;
	@GetMapping(path = "hello4")
	public String hello() {
		System.out.println(dbConfig.getUrl());
		System.out.println(dbConfig.getPassword());
		System.out.println(dbConfig.getUsername());
		return "hello";
	}
	
}
