package com.demo;
import java.util.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoContoller2 {
	
	@Value("${my.list.values}")
	private List<String> values;
	
	@Value("#{${my.todo}}")
	private Map<String, String> map;
	
	@GetMapping(path = "hello2")
	public String hello() {
		values.forEach(val-> System.out.println(val));
		map.forEach((k, v)-> System.out.println(k + ": "+ v));
		return "done";
	}
	
}
