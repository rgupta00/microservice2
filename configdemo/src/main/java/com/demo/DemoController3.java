package com.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController3 {

	@Value("${spring.datasource.url}")
	private String url;
	
	@Value("${spring.datasource.password}")
	private String password;
	
	@Value("${spring.datasource.username}")
	private String usrename;
	
	@Value("${spring.datasource.driver-class-name:not found}")
	private String driverName;
	//@GetMapping(path = "hello3")
	public String hello() {
		System.out.println(url);
		System.out.println(usrename);
		System.out.println(driverName);
		System.out.println(password);
		return "db connections";
	}
	
}
