package com.product.controller;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.product.service.Product;
import com.product.service.ProductService;

@RestController
public class ProductController {

	private ProductService productService;

	@Autowired
	public ProductController(ProductService productService) {
		this.productService = productService;
	}
	
	@GetMapping(path = "api/product")
	public List<Product> getAllProducts(){
		return productService.getAllProducts();
	}

	@GetMapping(path = "api/product/{id}")
	public Product getAnProducts(@PathVariable(name = "id") int id){
		return productService.getProductById(id);
	}
	
}
