<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Display all notes</title>
</head>
<body>
<table>
    <thead>
        <tr>
            <th>noteId</th>
            <th>title</th>
            <th>content</th>
            <th>status</th>
            <th>update</th>
            <th>delete</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach items="${notes}" var="note">
            <tr>
                <td>${note.noteId}</td>
                <td>${note.title}</td>
                <td>${note.content}</td>
                <td>${note.status}</td>
               
                <td><a href="updatenote?id=${note.noteId}">update note</a></td>
                <td><a href="deletenote?id=${note.noteId}">delete note</a></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<a href="addnote">addnote</a>
</body>
</html>