package com.noteapp.model;

import java.time.LocalDateTime;

public class Note {
	private int noteId;
	private String title;
	private String content;
	private String status;
	private LocalDateTime dateTime;
	
	public int getNoteId() {
		return noteId;
	}
	public void setNoteId(int noteId) {
		this.noteId = noteId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Note(int noteId, String title, String content, String status) {
		super();
		this.noteId = noteId;
		this.title = title;
		this.content = content;
		this.status = status;
		this.dateTime=LocalDateTime.now();
	}
	@Override
	public String toString() {
		return "Note [noteId=" + noteId + ", title=" + title + ", content=" + content + ", status=" + status
				+ ", dateTime=" + dateTime + "]";
	}
	public Note() {}
	
	
}
