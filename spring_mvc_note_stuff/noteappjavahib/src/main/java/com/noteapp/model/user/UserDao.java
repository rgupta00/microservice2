package com.noteapp.model.user;
import java.util.*;
public interface UserDao {
	public boolean isValid(String username, String password);
	public void addUser(User user);
	
}
