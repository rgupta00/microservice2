package com.noteapp.model.user;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public boolean isValid(String username, String password) {
		User user = (User) getSession()
				.createQuery("select u from User u where username=:username and password =:username")
				.setString("username", username).setString("password", password).getSingleResult();
		if (user != null)
			return true;
		else
			return false;
	}

	public void addUser(User user) {
		getSession().save(user);
	}

}
