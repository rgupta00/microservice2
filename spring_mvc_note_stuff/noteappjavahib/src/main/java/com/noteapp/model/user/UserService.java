package com.noteapp.model.user;

public interface UserService {
	public boolean isValid(String username, String password);
	public void addUser(User user);
	
}
