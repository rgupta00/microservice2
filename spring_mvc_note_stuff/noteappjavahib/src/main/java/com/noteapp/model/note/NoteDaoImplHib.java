package com.noteapp.model.note;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
@Repository
@Primary
public class NoteDaoImplHib implements NoteDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public boolean saveNote(Note note) {
		getSession().save(note);
		return true;
	}

	public boolean deleteNote(int noteId) {
		return false;
	}

	public List<Note> getAllNotes() {
		return getSession().createQuery("select n from Note n").getResultList();
	}

	public Note getNoteById(int noteId) {
		return null;
	}

	public boolean UpdateNote(Note note) {
		return false;
	}

}
