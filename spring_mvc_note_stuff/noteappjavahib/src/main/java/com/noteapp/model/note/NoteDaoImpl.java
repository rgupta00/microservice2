package com.noteapp.model.note;

import java.util.*;

import org.springframework.stereotype.Repository;
//@Repository
public class NoteDaoImpl implements NoteDao{
	
	private int counter=2;
	private Map<Integer, Note> notesMaps=new HashMap<Integer, Note>();
	
	public NoteDaoImpl() {
		notesMaps.put(1, new Note(1, "java", "baics java", "in progress"));
		notesMaps.put(2, new Note(2, "Spring", "Spring java", "in progress"));
		
	}
	public boolean saveNote(Note note) {
		 notesMaps.put(++counter, note);
		 return true;
	}

	public boolean deleteNote(int noteId) {
		return false;
	}

	public List<Note> getAllNotes() {
		return new ArrayList<Note>(notesMaps.values());
	}

	public Note getNoteById(int noteId) {
		return null;
	}

	public boolean UpdateNote(Note note) {
		return false;
	}

}
