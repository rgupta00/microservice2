<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add an new note</title>
</head>
<body>
<form:form method="post" action="addnote" modelAttribute="note">
	Enter id: <form:input path="noteId"/><br/>
	Enter title: <form:input path="title"/><br/>
	Enter content: <form:input path="content"/><br/>
	Enter status: <form:input path="status"/><br/>
	<input type="submit">
</form:form>
</body>
</html>