package com.noteapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.noteapp.model.Note;
import com.noteapp.service.NoteService;
//this is not a rest controller. this is based on jsp
@Controller
public class NoteController {
	
	@Autowired
	private NoteService noteService;
	@GetMapping(path = "allnotes")
	public ModelAndView showAll(ModelAndView mv) {
		mv.addObject("notes", noteService.getAllNotes());
		mv.setViewName("notesview");
		return mv;
	}

	@GetMapping(path = "addnote")
	public String addnoteGet(Model model) {
		model.addAttribute("note", new Note());
		return "addnote";	
	}
	
	@PostMapping(path = "addnote")
	public String addnotePost(@ModelAttribute(name = "note") Note note) {
		noteService.saveNote(note);
		System.out.println("a new node is added");
		return "redirect:/allnotes";	
	}
}
