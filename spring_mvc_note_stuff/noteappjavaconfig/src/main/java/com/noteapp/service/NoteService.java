package com.noteapp.service;

import java.util.List;

import com.noteapp.model.Note;

public interface NoteService {
	public boolean saveNote(Note note);

	public boolean deleteNote(int noteId);

	public List<Note> getAllNotes();

	public Note getNoteById(int noteId);

	public boolean UpdateNote(Note note);
}
