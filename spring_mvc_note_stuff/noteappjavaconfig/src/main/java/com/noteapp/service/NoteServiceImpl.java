package com.noteapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.noteapp.model.Note;
import com.noteapp.model.NoteDao;

@Service
@Transactional
public class NoteServiceImpl implements NoteService {
	@Autowired
	private NoteDao noteDao;

	public boolean saveNote(Note note) {
		return noteDao.saveNote(note);
	}

	public boolean deleteNote(int noteId) {
		return false;
	}

	public List<Note> getAllNotes() {
		return noteDao.getAllNotes();
	}

	public Note getNoteById(int noteId) {

		return null;
	}

	public boolean UpdateNote(Note note) {

		return false;
	}

}
