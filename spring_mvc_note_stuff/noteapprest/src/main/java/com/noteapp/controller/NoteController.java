package com.noteapp.controller;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.noteapp.model.note.Note;
import com.noteapp.service.NoteService;
//this is not a rest controller. this is based on jsp
@RestController
public class NoteController {
	
	@Autowired
	private NoteService noteService;
	
	@GetMapping(path = "note", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Note> showAll() {
		return noteService.getAllNotes();
	}

	
	
	@PostMapping(path = "note",
			 consumes  = MediaType.APPLICATION_JSON_VALUE)
	public String addnote(@RequestBody Note note) {
		noteService.saveNote(note);
		return "Added successfully";
	}
}
