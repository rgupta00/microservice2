package com.noteapp.model.note;

import java.util.List;

public interface NoteDao {

	public boolean saveNote(Note note);

	public boolean deleteNote(int noteId);

	public List<Note> getAllNotes();

	public Note getNoteById(int noteId);

	public boolean UpdateNote(Note note);
}
