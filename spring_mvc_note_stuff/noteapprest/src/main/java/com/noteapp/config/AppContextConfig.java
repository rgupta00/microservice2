package com.noteapp.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
//this code is used to configure hibernate sessionfactory and declative tx mgt
//in spring boot life is so easy we just need to configure some properties :(
@Configuration
@ComponentScan(basePackages = {"com.noteapp"})
@EnableTransactionManagement
@PropertySource(value = "classpath:db.properties")
public class AppContextConfig {

	@Autowired
	private Environment environment;
	
	private DataSource getDataSource() {
		DriverManagerDataSource ds=new DriverManagerDataSource();
		ds.setDriverClassName(environment.getProperty("driver"));
		ds.setUrl(environment.getProperty("url"));
		ds.setPassword(environment.getProperty("password"));
		ds.setUsername(environment.getProperty("username"));
		return ds;
	}
	
	@Bean
	public LocalSessionFactoryBean getSessionFactory() {

		LocalSessionFactoryBean lsfb = new LocalSessionFactoryBean();
		lsfb.setDataSource(getDataSource());
		lsfb.setPackagesToScan("com.noteapp.model");
		lsfb.setHibernateProperties(getHibernateProperties());
		return lsfb;
	}
	@Bean
	public Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", "update");
		properties.setProperty("hibernate.dialect",
				"org.hibernate.dialect.MySQL57Dialect");
		properties.setProperty("hibernate.show_sql", "true");
		properties.setProperty("hibernate.formate_sql", "true");
		return properties;
	}

	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory factory) {
		HibernateTransactionManager tmx = new HibernateTransactionManager();
		tmx.setSessionFactory(factory);
		return tmx;
	}
}
